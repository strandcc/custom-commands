# Utility Commands
The commands listed here are common custom commands that are among the most requested.

## Guide Markdown
We assume you are using the default command prefix, `?`. Commands with inputs are denoted as ``?command [input]``. Commands with choice inputs are denoted as ``?command [input1/input2]``. Commands with optional inputs are denoted as ``?command (input)``.

## Commands
* `?say [channel] [message]` - Makes Dyno say your message.  
* `?dm [user] [message]` - Sends a direct message to the specified user.
* `?dashboard [optional module]` - Displays a link to your server's Dyno dashboard.

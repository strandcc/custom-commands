# Custom Commands

This repository contains commands and modules dedicating to improving the quality of your server. Just pick one of your choice!

## Additional Resources
[Our Discord Server](https://discord.gg/XqPXMVt)

[Dyno Support Server](https://discord.gg/Dyno)

[Our Dyno Wiki Page](https://wiki.dyno.gg/modules/ccs/complex)

## Team
**soda#0001** - Owner / Development Manager

**bean#0001** - Research Manager

**Rеd#0001** - Research Manager

**Decease#0001** - Team Member

**egrets#9999** - Project Advisor

**Braydon#6223** - Project Advisor

---------
**Sword#0042** - Contributor

**Clamori#2411** - Contributor

----------
**Sobriquet#0001** - Founder / Owner Emeritus



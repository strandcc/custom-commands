{choose:{urlencode:$1+
}
{!announce {channel} **You searched for** ``$1+``**.**

__**Search Results**__
[Google](http://www.google.com/search?q={choice})
[Wikipedia](https://en.wikipedia.org/w/index.php?title=Special:Search&search={choice})
[Spotify](https://open.spotify.com/search/{choice})
[YouTube](https://www.youtube.com/results?utm_source=opensearch&search_query={choice})
}

# Dyno Jenga 
This set of commands forms a basic emulation of the classic board game Jenga, using only custom commands. It was created by Clamori#2411 during a Proficient Challenge on December 1, 2019.

**Note:** Dyno Premium is required for this module, due to the usage of Additional Responses.

## Commands

`?jenga` - Displays information about the module, such as commands.

`?build [#]` - Builds the Jenga tower users will be playing on.

`?viewtower [ID/Blocks]` - Displays basic information about a Jenga tower.

`?addblock [ID]` - Adds blocks to a Jenga tower.

`?remove [ID]` - Removes blocks from a Jenga tower.

`?checkid [Blocks]` - Displays the ID of a Jenga tower based on how many blocks are currently in that tower.

`?collapse [Blocks]` - Deletes a tower.


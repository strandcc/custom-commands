# Entertainment Commands  
General commands for entertainment.

## Guide Markdown  
We assume you are using the default command prefix, `?`. Commands with inputs are denoted as ``?command [input]``. Commands with choice inputs are denoted as ``?command [input1/input2]``. Commands with optional inputs are denoted as ``?command (input)``.

## Commands
* `?8ball [question]` - Ask the Magic 8 Ball a question! 
* `?hug [user]` - Hug another user.
* `?rate [user]` - Rates another user on a scale of 1 - 100. 
* `?muteroulette` - A fun roulette game that has a chance of muting you!

*To see the commands in action, visit the [Wiki](https://github.com/Strand-Custom-Commands/Strand-Custom-Commands/wiki).*

### Notes
Admins, Moderators, and Protected Roles can't use `?muteroulette` since they're protected from mod commands.
